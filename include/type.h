/*date:
12/04/2015
auteurs:
Alexis Raguenes
Florian Pierre-louis
*/

//Projet de CPS: Liste alphabétique des mots d'un texte
#ifndef type

#define type

typedef struct maillon_t maillon_t;
struct maillon_t {
   	unsigned int corp;
    maillon_t* queue ; 
};

typedef  struct emplacement_t emplacement_t;
struct emplacement_t
{
	unsigned int ligne ;
	unsigned int collone ;
    emplacement_t* queue ; 
};

typedef struct mot_t mot_t;
struct mot_t{
	maillon_t* tete_maillon;
	maillon_t* queue_maillon;
	emplacement_t* tete_liste;
	//emplacement_t* queue_liste;// pour le moment je fait sans. 
	mot_t* queue;
};
#endif
 
//à quoi ça sert d'enregistré le dernier maillon d'une liste simplement chaîné ?
