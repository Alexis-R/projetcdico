/*date:
12/04/2015
auteurs:
Alexis Raguenes
Florian Pierre-louis
*/

//Projet de CPS: Liste alphabétique des mots d'un texte
#ifndef MANIPDICO

#define MANIPDICO
//convertie un char en un int de 1 (a) à 26 (z)
unsigned int char_to_num( char c);
//convertie un int en char 
char num_to_char(unsigned int i);
//
void set_charnum(maillon_t* maillon,int num,char c);
//
char get_charnum(maillon_t* maillon,int num);
//convertie une chaine str en maillon,renvoie le dernier maillon
maillon_t* str_list(char* str,maillon_t* maillon);
//restitue la chaine contenue dans un maillon(ou une liste chaîné de maillon )
//str est alloué dans la fonction
char* list_str(maillon_t* maillon);
//sauvegarde un mot,il n'est pu utile de garder str ,nbligne et nbcol après mais unMot doit être garder
mot_t* save_mot(char* str,unsigned int* nbligne,unsigned int* nbcol);
void disp_mot(mot_t* unMot);
int compare_mots(mot_t* mot1,mot_t* mot2);
//insère un mot dans le dictionnaire,le dico fait déjà un mot avant appelle 
mot_t* insertion_dictionnaire(mot_t* monDico,mot_t* monMot);
//afichifage du dico ,
void disp_dico(mot_t* monDico);




#endif
 
