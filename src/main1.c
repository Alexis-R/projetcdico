/*date:
12/04/2015
auteurs:
Alexis Raguenes
Florian Pierre-louis
*/
//Alexis Raguenes
//Projet de CPS: Liste alphabétique des mots d'un texte
//modifié pour faire des test 
//exercice d'entraînement 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read_word.h"

#define TEXTE "../Examples/hugo.txt" 		
int main()								
{													
    FILE* fichier = NULL;		
    fichier = fopen(TEXTE, "r");		
	char carac[1000];				
								
	int nblin;		
	int nbcol;	
	printf("test taille %d \n",sizeof(carac));
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");

	do{		
		strcpy(carac ,  next_word(fichier,&nblin,&nbcol) ); 	
		printf("%d\n",strlen(carac));
		if (carac[3] =='/0'){printf("carac de fin \n");}else{printf("pas de fin \n");}
			printf("%s (%d,%d)\n",carac,nblin,nbcol);				
			printf("%d \n",feof(fichier));				
	}while(! feof(fichier));			
									
	fclose(fichier);
    return 0;
}

