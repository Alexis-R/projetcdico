/*date:
12/04/2015
auteurs:
Alexis Raguenes
Florian Pierre-louis
*/
//Alexis Raguenes
//Projet de CPS: Liste alphabétique des mots d'un texte

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "type.h"

//fonction d'aide pour test->permet d'affiché les bit 
void affichebin(unsigned n)
{
	int i;
	unsigned bit = 0 ;
	unsigned mask = 1 ;
	for ( i = 0 ; i < 32 ; ++i)
	{
		bit = (n & mask) >> i ;
		printf("%d", bit) ;
		mask <<= 1 ;
	}
}
///////

unsigned int char_to_num( char c){
	return (c -'a'+1);//ascii commence a 0 nous on veut commercer à 1 donc on soustrait 'a' à notre caractère et on ajoute 1, pour 'a' -> 'a'-'a'+1 on à bien 1
}

char num_to_char(unsigned int i){
	return (i+'a'-1);//on commence a compté à 1 ,a->1 donc si i=1 on fait -1 et on renvoi 1 ,ainsi de suite pour les 26 lettre de l'alphabet qui se suivent dans la table ascii
}

//met un caractère dans un maillon à la position indiqué, (pour être stocké le caractère est stocké en nombre sur 5 bit)
void set_charnum(maillon_t* maillon,int num,char c){
	
	 unsigned int decalage = 25-(5*num );
	 unsigned int valeur = char_to_num(c);
	 unsigned int masque = 31 << decalage;
	//affichebin(maillon->corp);//
	(maillon->corp) = (maillon->corp) & ~(masque);
	valeur = (valeur << decalage);
	(maillon->corp) = (maillon->corp) | (valeur);
	//affichebin(maillon->corp);//
}
	
//recupère le caractère à un emplacement précis du maillon ,5 bit devinent un char
char get_charnum(maillon_t* maillon,int num){
	unsigned int decalage = 25-(5*num);
	unsigned int masque = 31 << decalage;
	masque = (maillon->corp) & masque;
	masque = masque >> decalage;
	return num_to_char(masque);
}


maillon_t* str_list(char* str,maillon_t* maillon){//on ecrit la chaîne de caractère dans un maillon
	int taille = (strlen(str));//on compte le nombre de caractères avant un \0
	maillon_t* monMaillon = maillon;
	int i = 0;
	int i2 =0;
	while(i<taille){
		(monMaillon->corp) = 0;
		monMaillon->queue = NULL;//on met le pointeur de la queue à null pour marquer la fin de la chaîne si la chaîne ne se finir pas à cette itération le 2eme if donne une valeur a maillon->queue
		for(i2=0;i2<=5;i2++){//on remplie le premier maillon ,6 lettre ou moins si on dépasse i on sort de la boucle for
			if( i <taille){
			set_charnum(monMaillon,i2,str[i]);
			}
			i++;
		} 
		if( i <=taille){
			monMaillon->queue = malloc(sizeof(maillon_t)); // Allocation de la mémoire
   				 if (monMaillon->queue == NULL){printf("catastrophe");exit(1);}
		monMaillon = monMaillon->queue;//le maillon actif devient celui pointé par l'adresse de la queue
		}
		}
	return  monMaillon;//on renvoie mon  maillon qui correspond à la queue de la liste chaîné de maillon ,cella servira à l'enregistrer dans mot_t
}//il n'y à pas de caractère pour marqué la fin du mot ,juste des 0


char* list_str(maillon_t* maillon){//on enregistre le mot d'un maillon dans une chaîne,
	char* str;
	maillon_t* monMaillon = maillon;
	int i2=0;
	int i = 0;
	char buff[500];//buff est temporaire à la fonction
	
	do{	
		for(i2=0;i2<=5;i2++){//on li le premier maillon		
				buff[i] = get_charnum( monMaillon,i2);
				if(buff[i] == 'a'-1){
					buff[i] = '\0';
					}
				i++;

			}
		monMaillon = monMaillon->queue;//le maillon actif devient celui pointé par l'adresse de la queue
		
	}while(monMaillon != NULL);//fin de la liste chainé
	buff[i]= '\0';
	str =malloc((strlen(buff) * sizeof(char))+1);//on alloue la mémoire pour str ,de la taille de la chaîne +1 pour \0
	strcpy(str,buff);

	//on récupère la chaîne de buff et on la met dans str,strcopy s'arête quand il rencontre le \0 ,(il copie aussi le \0)
	return str;
}


mot_t* save_mot(char* str,unsigned int* nbligne,unsigned int* nbcol){/*enregistre un mot(la première apparition de ce mot UNIQUEMENT ) -> renseigne le premier et dernier maillon,renseigne l'emplacement, */
	//free(unMot);
	mot_t* unMot;
	unMot = malloc((sizeof(mot_t)));
	if(unMot == NULL){printf("ereure save mot pointeur unMot\n");}
	unMot->tete_maillon = malloc(sizeof(maillon_t));//on initialise le 1er maillon
	unMot->queue_maillon = str_list(str,(unMot->tete_maillon));//remplie les champs maillon de unMot
	unMot->tete_liste = malloc(sizeof(emplacement_t));//on alloue le premier emplacement (les emplacement sont stocké dans une liste chaîné emplacement_t)
	(unMot->tete_liste)->ligne = *nbligne;
	(unMot->tete_liste)->collone = *nbcol;
	(unMot->tete_liste)->queue = NULL; 
	return unMot;
}

 
void disp_mot(mot_t* unMot){//affiche un mot ,ne renvoie rien,affiche sur la sortie standard le mot et les emplacements associé
	char* txtMot;//le mot sera alloué dans list_str
	mot_t* LEmot = 	unMot;
	emplacement_t* parcour =  (LEmot->tete_liste);
	txtMot = list_str(LEmot->tete_maillon);//on récupère le mot dans mot_t par la fonction list_str(qui gère le parcours de maillon etc)
	printf("%s",txtMot);//le mot est une chaîne de caractère affichage,
	do{
		printf("(%d,%d)  ",parcour->ligne,parcour->collone);//on affiche l'emplacement ligne,colonne
		parcour = parcour->queue;//les emplacement sont en liste chaîné ,
	}while(parcour != NULL);//la queue est null c'est al fin de la liste
	printf("\n");
}



int compare_mots(mot_t* mot1,mot_t* mot2){//compare deux mot
	/*renvoie 0 si identique,un entier négatif si mot1 est alphabétiquement plus petit et un entier positif si le 1er mot est alphabétiquement plus grand
	*/
	//ou utilise la fonction fournie string.h pour comparé eux chaîne de caractère
	//on utilise la fonction list_str pour récupérer les deux chaîne de caractère.
	char* str1;
	char* str2; 
	str1 = list_str(mot1->tete_maillon);
	str2 = list_str(mot2->tete_maillon);
	return (strcmp(str1,str2));
}


void ajoutEmplacement(mot_t* motSource,mot_t* unMot){
		emplacement_t* tempEmplacement = (motSource->tete_liste);//on stocke la liste des emplacement dans une autre valeur pour pouvoir la parcourir. 
		while((tempEmplacement->queue) != NULL ){
				tempEmplacement = (tempEmplacement->queue);		
		}
		emplacement_t* newEmplacement = malloc(sizeof(emplacement_t));
		tempEmplacement->queue = newEmplacement;
		newEmplacement->queue = NULL;
		newEmplacement->ligne = (unMot->tete_liste)->ligne;
		newEmplacement->collone = (unMot->tete_liste)->collone;
			//free(unMot);

}


//on ne vérifie pas le classement des mot déjà présent dans le dictionnaire ,pour être sûr du classement après insertion if faut être sûr du classement de monDico avant insertion 
mot_t* insertion_dictionnaire(mot_t* monDico,mot_t* monMot){//ajoute un mot dans le dictionnaire (liste chaîné de mots)
	
	int place = 0;//place servira de variable pour la boucle while du placement
	mot_t* DicoActif = monDico;
	int pos = 0;//sert a indiqué ou le nouveau mot se situe par rapport au mot regarder dans le dico
	pos = compare_mots(DicoActif,monMot);//on test si notre mot va être le premier mot du dictionnaire 
	//si le on insère le mot à la place du 1er mot du dictionnaire,qui sert aussi de référence  au dictionnaire tout entier,il faut changer monDico
	if (pos > 0){
			monMot->queue = DicoActif;
			monDico = monMot;
			//si le on insère le mot à la place du 1er mot du dictionnaire,qui sert aussi de référence  au dictionnaire tout entier,il faut changer monDico
		}
	else if(pos == 0){//mot identique ,on ajoute l' emplacement
			ajoutEmplacement(DicoActif,monMot);		
			place = 22;
		
		}
	else{

	while(place ==0){
		
		if(DicoActif->queue == NULL ){//un seul mot dans le dico
			pos = compare_mots(DicoActif,monMot);//on test si notre mot va être le 2eme mot du dictionnaire 
			if(pos == 0){//mot identique ,on ajoute l' emplacement
				ajoutEmplacement(DicoActif,monMot);
				place = 22;
			}
			else if(pos < 0){//un seul mot,nouveau mot plus petit
				(DicoActif->queue)= monMot;
				monMot->queue = NULL;
				place = 12;
			}
			else {
				printf("problème d'insertion dans un dico de 1 mot"); 
			}
		}
		else{
			pos = compare_mots((DicoActif->queue),monMot);
			if (pos > 0 && place == 0){
				//on a trouver l'emplacement du mot,
				(monMot->queue) = DicoActif->queue;
				DicoActif->queue = monMot;
				place = 1;
		}
		else if(pos == 0 && place == 0 ){//mot identique ,on ajoute l' emplacement
			ajoutEmplacement((DicoActif->queue),monMot);
			place = 2;
		}


		else if(pos < 0 && (((DicoActif->queue)->queue) == NULL ) && place == 0 ){//on à fini le parcours de la liste donc on met le mot à la fin
			(DicoActif->queue)->queue = monMot;
			monMot->queue = NULL;
			place = 3;
		}
		else if(pos < 0 && place == 0 ){//on continue de parcourir la liste chaîné 
			DicoActif = DicoActif->queue;
		}
		else{
			printf("erreur dans insertion dico \n");
			exit(3);
		}
	}	
	}
	}//premier else pour le cas ou on ne change pas le premier mot
	return monDico;
}

void disp_dico(mot_t* monDico){//affiche sur la sortie standard le dico
	mot_t* DicoActif = monDico;
	do{
		disp_mot(DicoActif);
		DicoActif = DicoActif->queue;
	}while((DicoActif != NULL ));	
}

