/*date:
12/04/2015
auteurs:
Alexis Raguenes
Florian Pierre-louis
*/
//Alexis Raguenes
//Projet de CPS: Liste alphabétique des mots d'un texte

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read_word.h"
#include "type.h"
#include "manipDico.h"

#define TEXTE "../Examples/orwell.txt" 		

int main(){

  	  FILE* fichier = NULL;		
    	fichier = fopen(TEXTE, "r");		

		mot_t* motN1;//ce mot sert de dico
		mot_t* tempMot;
		char caracBuff[1000];								
		unsigned int nblin;		
		unsigned int nbcol;	

		printf("mots et leur positions dans l'ordre d' apparition : \n");
		strcpy(caracBuff,next_word(fichier,&nblin,&nbcol) );
		motN1 = save_mot(caracBuff,&nblin,&nbcol);
		disp_mot(motN1);
		motN1->queue = NULL ;

	if(! feof(fichier)){
	do{	
		strcpy(caracBuff,next_word(fichier,&nblin,&nbcol) );
		tempMot = save_mot(caracBuff,&nblin,&nbcol);
		disp_mot(tempMot);
		motN1 = insertion_dictionnaire(motN1,tempMot);		
	}while(! feof(fichier));			
	
	}									
	fclose(fichier);
	printf("\n");
	printf("Dictionnaire classé par ordre alphabétique :\n\n");
	disp_dico(motN1);


//fin du programme

//tests des fonctions:

//on test les masque 
/*
	char c ='c';
	int mapos = 4;
	char machaine;
	printf("%d valeur de c \n",(c -'a'+1));
	maillon_t* theMaillon;
	theMaillon = malloc(sizeof(maillon_t));
	(theMaillon->corp) = 8548;
	set_charnum(theMaillon,mapos, c);
	machaine = get_charnum(theMaillon,mapos);
	printf("machaine :%c \n",machaine);
*/
//l'écriture marche,enfin au niveaux des masque c'est OK,

//écriture d'une chaîne dans un maillon (et la récupéré bien sûr) 
/*
	maillon_t* theMaillon;
	theMaillon = malloc(sizeof(maillon_t));
	//(theMaillon->corp) = 0;
	char* maStr = "voilavoilaoo";
	printf("maStr :%s \n",maStr);
	str_list(maStr, theMaillon);
	printf("mblabla\n");
	char newchar = get_charnum(theMaillon,0);
	char* maChaine;
	maChaine = list_str(theMaillon);
	printf("carac 2 %c \n",get_charnum( theMaillon,2));
	printf("maChaine :%s \n",maChaine);
*/
//l'écriture marche aussi aux niveaux de la gestion liste str <-> liste maillon

//test sur les mot_t
//fin de tests	
	return 0;
}
